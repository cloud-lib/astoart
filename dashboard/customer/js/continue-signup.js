$(document).ready(function(){
	$("#smartwizard").smartWizard({
		selected          : 0,
		keyNavigation     : false,
		autoAdjustHeight  : true,
		cycleSteps        : false,
		backButtonSupport : false,
		useURLhash        : false,
		lang: {
			next     : "Lanjut",
			previous : "Kembali"
		},
		theme            : "dots",
		transitionEffect : "fade",
		transitionSpeed  : "400"
	});
});

var baseUrl = 'https://dashboard.astoart.com/request/';

function loadNowProvince(){
 	$.ajax({
 		url      : baseUrl + 'address/province',
 		method   : 'GET',
 		data     : {cc : $('#ajax_country').val()},
 		async    : true,
 		dataType : 'json',
 		success  : function(data){
 			var html = '<option hidden="">Provinsi</option>';
 			var i;

 			for(i=0; i<data.length; i++){
 				html += '<option value="'+data[i].id+'">'+data[i].province+'</option>';
 			}
 			$('#ajax_province').html(html);
 		}
 	});
}

function loadNowDistrict(){
	$.ajax({
		url      : baseUrl + 'address/district',
		method   : 'GET',
		data     : {cc : $('#ajax_country').val() , id : $('#ajax_province').val()},
		async    : true,
		dataType : 'json',
		success  : function(data){
			var html = '<option hidden="">Kab. / Kota</option>';
			var i;

			for(i=0; i<data.length; i++){
				html += '<option value="'+data[i].id+'">'+data[i].district+'</option>';
			}
			$('#ajax_district').html(html);
		}
	});
}

function loadNowSubDistrict(){
	$.ajax({
		url      : baseUrl + 'address/sub_district',
		method   : 'GET',
		data     : {cc : $('#ajax_country').val() , id : $('#ajax_district').val()},
		async    : true,
		dataType : 'json',
		success  : function(data){
			var html = '<option hidden="">Kecamatan</option>';
			var i;

			for(i=0; i<data.length; i++){
				html += '<option value="'+data[i].id+'">'+data[i].sub_district+'</option>';
			}
			$('#ajax_sub_district').html(html);
		}
	});
}

function loadNowVillage(){
	$.ajax({
		url      : baseUrl + 'address/village',
		method   : 'GET',
		data     : {cc : $('#ajax_country').val() , id : $('#ajax_sub_district').val()},
		async    : true,
		dataType : 'json',
		success  : function(data){
			var html = '<option hidden="">Desa</option>';
			var i;

			for(i=0; i<data.length; i++){
				html += '<option value="'+data[i].id+'">'+data[i].village+'</option>';
			}
			$('#ajax_village').html(html);
		}
	});
}